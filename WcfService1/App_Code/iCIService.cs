﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


[ServiceContract]
public interface iCIService
{
    [OperationContract]
        Dictionary<string, string> GetCI(string value);

    [OperationContract]
        string SetCI(Dictionary<string, string> value);
   
}