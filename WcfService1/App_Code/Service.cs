﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;


public class Service : iCIService
{
    public Dictionary<string, string> GetCI(string value)
    {
        handler ps = new handler();
        //Dictionary<string, string> rez = ps.GetCIdata("111444serial");
        Dictionary<string, string> rez = ps.GetCIdata(value); 
        return rez;
    }

    
    public string SetCI(Dictionary<string, string> value)
    {
        handler psSet = new handler();
        string rez = psSet.SetCIdata(value);
        return rez;

    }

}
