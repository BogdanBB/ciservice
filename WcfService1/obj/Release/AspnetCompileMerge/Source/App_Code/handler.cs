﻿using System.Collections.Generic;
using System.Linq;
//using System.Management.Automation; //для  powershell
using Microsoft.EnterpriseManagement.Common;  // for EnterpriseManagementObjectBaseWithProperties obj;
using Microsoft.EnterpriseManagement;  // for EnterpriseManagementGroup emg =
using Microsoft.EnterpriseManagement.Configuration; //for  ManagementPackClassCriteria classCr
using System.Globalization; // for spec culture
using System.Windows.Forms;
using System.Data.SqlClient;
using System;

public class handler
{
  private CultureInfo SpecCult = new CultureInfo("ru-RU", false);

     private static EnterpriseManagementGroup  ConnMG()
    {
        //get data from conn string
        System.Configuration.Configuration rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
        System.Configuration.ConnectionStringSettings connString = rootWebConfig.ConnectionStrings.ConnectionStrings["SCSMConnString"];

        string connStringSTR = connString.ConnectionString;
         SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connStringSTR);
        string userConnStr = builder.UserID;
        string passConnStr = builder.Password;
        string serverConnStr = builder.DataSource;



        EnterpriseManagementConnectionSettings settings = new EnterpriseManagementConnectionSettings(serverConnStr);
        settings.UserName = userConnStr;
        settings.Password = new System.Security.SecureString();
        foreach (char c in passConnStr)
            settings.Password.AppendChar(c);
        settings.Domain = "imb.local";
        settings.InactivityTimeout = 1; //in minutes
        

        /* this block work fine! try with conn string
        EnterpriseManagementConnectionSettings settings = new EnterpriseManagementConnectionSettings("imb-scsmconact");
        //Test020
        //1qaz@WSX
        settings.UserName = "Test020";
        char[] pass = { '1', 'q', 'a', 'z', '@', 'W', 'S', 'X' };
        settings.Password = new System.Security.SecureString();
        foreach (char c in pass)
            settings.Password.AppendChar(c);
        settings.Domain = "imb.local";
        settings.InactivityTimeout = 2; //2 -minutes
        */
                
        EnterpriseManagementGroup emg = new EnterpriseManagementGroup(settings);
       
        return emg;
    }


    public Dictionary<string, string> GetCIdata(string SerialNo)
	{
        Dictionary<string, string> CImemders = new Dictionary<string, string>();
        CImemders.Clear();
        EnterpriseManagementGroup Mangrp = ConnMG();

        ManagementPackClassCriteria classCr = new ManagementPackClassCriteria("Name = 'PtB.CCI.Display'");
        IList<ManagementPackClass> classes = Mangrp.EntityTypes.GetClasses(classCr);

        if (classes != null && classes.Count > 0)
        {
            ManagementPackClass cl = classes[0];

            EnterpriseManagementObjectCriteria cr = new EnterpriseManagementObjectCriteria("sn = " +"'"+ SerialNo+"'", cl); 
            IObjectReader<EnterpriseManagementObject> reader = Mangrp.EntityObjects.GetObjectReader<EnterpriseManagementObject>(cr, ObjectQueryOptions.Default);
            if (reader != null && reader.Count > 0)
            {
                EnterpriseManagementObject obj = reader.First();

                    int n = obj.Values.Count;

                for (int nn = 0; nn < n; nn++) {
                    if (obj.Values[nn].Value != null)
                    {
                        //CImemders.Add(obj.Values[nn].Type.DisplayName.ToString() + " " + nn.ToString(), obj.Values[nn].Value.ToString()); // original 

                        if (obj.Values[nn].Value.ToString().Contains("rtf1"))  // проверка на RFT 
                        {  // ртф строка 

                            RichTextBox rtBox = new RichTextBox();
                            rtBox.Rtf = obj.Values[nn].Value.ToString();
                            CImemders.Add(obj.Values[nn].Type.ToString() + ";" + obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), rtBox.Text);
                            rtBox.Clear();

                        }

                        else
                        { // обычная строка plain text

                            CImemders.Add(obj.Values[nn].Type.ToString() + ";" + obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), obj.Values[nn].Value.ToString());

                        }

                    }
                    else {
                             CImemders.Add(obj.Values[nn].Type.ToString() + ";" + obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), ""); //original
                    }

                }
            }
        }


        if (CImemders.Count == 0)
        {

             classCr = new ManagementPackClassCriteria("Name = 'PtB.CCI.Laptop'");
            
            classes = Mangrp.EntityTypes.GetClasses(classCr);

            if (classes != null && classes.Count > 0) //есть ли инстансы классов
            {
                ManagementPackClass cl = classes[0];

                EnterpriseManagementObjectCriteria cr = new EnterpriseManagementObjectCriteria("sn = " + "'" + SerialNo + "'", cl);
                 
                IObjectReader<EnterpriseManagementObject> reader = Mangrp.EntityObjects.GetObjectReader<EnterpriseManagementObject>(cr, ObjectQueryOptions.Default);
                if (reader != null && reader.Count > 0)
                {
                    EnterpriseManagementObject obj = reader.First();

                    int n = obj.Values.Count;

                    for (int nn = 0; nn < n; nn++)
                    {
                        if (obj.Values[nn].Value != null) // проверка на пустые строки
                        {// для непустых строк
                            //CImemders.Add(obj.Values[nn].Type.DisplayName.ToString() + " " + nn.ToString(), obj.Values[nn].Value.ToString()); // original 
                            if (obj.Values[nn].Value.ToString().Contains("rtf1"))  // проверка на RFT 
                              {  // ртф строка 

                                RichTextBox rtBox = new RichTextBox();
                                rtBox.Rtf = obj.Values[nn].Value.ToString();

                                CImemders.Add(obj.Values[nn].Type.ToString() + ";" + obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), rtBox.Text);

                                 rtBox.Clear();

                            }
                            
                            else { // обычная строка

                            CImemders.Add(obj.Values[nn].Type.ToString()+";"+obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), obj.Values[nn].Value.ToString());

                                   }
                           

                        }
                        else
                        {// для пустых строк
                            CImemders.Add(obj.Values[nn].Type.ToString() + ";" + obj.Values[nn].Type.GetDisplayString(SpecCult).Name.ToString(), ""); //original
                        }

                    }
                }
            }
           
        }

        return CImemders;


        //  блок Подключение
        /*      EnterpriseManagementConnectionSettings settings = new EnterpriseManagementConnectionSettings("imb-scsmconact");
              //Test020
              //1qaz@WSX
              settings.UserName = "Test020";
          char[] pass = { '1', 'q', 'a', 'z', '@', 'W', 'S', 'X' };
          settings.Password = new System.Security.SecureString();
              foreach (char c in pass)
                  settings.Password.AppendChar(c);
              settings.Domain = "imb.local";
              EnterpriseManagementGroup emg = new EnterpriseManagementGroup(settings);
              // блок Подключение
              */

        /* 
       //Использование PowerShell
       string result = "";
       string handler = "import-module smlets;(Get-SCSMObject -ComputerName imb-scsmconact  -Class  (Get-SCSMClass -ComputerName imb-scsmconact  -Name PtB.CCI.Display$) -Filter {sn -like '" + SerialNo + "' }).values";
        */

        /*
        // C# Получение объекта по внутреннему ID (Guid)
        EnterpriseManagementObject obj =
            emg.EntityObjects.GetObject<EnterpriseManagementObject>(
                new Guid("9eca0414-c0a3-3587-3b5e-25ef75600194"), ObjectQueryOptions.Default);
         
        //c# Получение объектов по строковому критерию и классу
         */


        /* 
        //=====Использование PowerShell  

        var shell = PowerShell.Create();
        // Add the script to the PowerShell object
        //shell.Commands.AddScript(PowerShellCodeBox.Text);
        shell.Commands.AddScript(handler);
        // Execute the script
        var results = shell.Invoke();
        // display results, with BaseObject converted to string
        // Note : use |out-string for console-like output
        if (results.Count>0)
        {
            // We use a string builder ton create our result text
            var builder = new StringBuilder();
            
            int k = 0;
            //EnterpriseManagementObjectBaseWithProperties obj;
            

            foreach (var psObject in results)
            {

                                        
                // Convert the Base Object to a string and append it to the string builder.
                // Add \r\n for line breaks
                if (psObject != null) 
                {

                builder.Append(psObject.BaseObject.ToString()+ "\r\n");
                    CImemders.Add(k.ToString(), psObject.ToString());
                    k++;
                    
                }
                else 
                {
                    builder.Append("SN not found");
                }
            }
            // Encode the string in HTML (prevent security issue with 'dangerous' caracters like < >
            result = builder.ToString();

        }
        //CImemders.Add("99999", obj.DisplayName.ToString());
        //return result;

         //====Использование PowerShell
         */
    }


    
    
    public string SetCIdata ( Dictionary<string,string>CIData )
     {

//to method FindItem()
        string sSN;
        string snkey = "sn";

        Dictionary<string, string> dsCustCIkeyVal = new Dictionary<string, string>();
        dsCustCIkeyVal.Clear();
        EnterpriseManagementGroup emg = ConnMG();
        dsCustCIkeyVal = CIData;
        try
        {
            sSN = dsCustCIkeyVal[snkey];
        }
        catch (System.Collections.Generic.KeyNotFoundException)
        {
            return "Serial key not found";
        }
        

       
        //Get My custom cunfig items mp  id=336de622-d7b2-b908-d559-58828d5b4685
        ManagementPack mpCustonConfItems = emg.ManagementPacks.GetManagementPack(new Guid("336de622-d7b2-b908-d559-58828d5b4685"));

        //Get the system.workitem class
        /*
DisplayName Name               ManagementPackName     Id

Монитор      PtB.CCI.Display    PtB.CCI                bd52e36d-4d4b-7f17-b5fe-12278a9c8543
Ноутбук      PtB.CCI.Laptop     PtB.CCI                 352c8f93-c61d-27fe-457e-e12a379e2286
         */
        ManagementPackClass mpcCustomConfItem = emg.EntityTypes.GetClass(new Guid("352c8f93-c61d-27fe-457e-e12a379e2286"));

      
        string strCustCISearchCriteria = "";

        strCustCISearchCriteria = System.String.Format(@"<Criteria xmlns=""http://Microsoft.EnterpriseManagement.Core.Criteria/"">" +
                  "<Expression>" +
                  "<SimpleExpression>" +
                      "<ValueExpressionLeft>" +
                      "<Property>$Context/Property[Type='PtB.CCI.Laptop']/sn$</Property>" +
                      "</ValueExpressionLeft>" +
                      "<Operator>Equal</Operator>" +
                      "<ValueExpressionRight>" +
                      "<Value>" + sSN + "</Value>" +
                      "</ValueExpressionRight>" +
                  "</SimpleExpression>" +
                  "</Expression>" +
              "</Criteria>");

        EnterpriseManagementObjectCriteria emocWorkitem = new EnterpriseManagementObjectCriteria((string)strCustCISearchCriteria, mpcCustomConfItem, mpCustonConfItems, emg);
        IObjectReader<EnterpriseManagementObject> readerWorkitem = emg.EntityObjects.GetObjectReader<EnterpriseManagementObject>(emocWorkitem, ObjectQueryOptions.Default);
        EnterpriseManagementObject emoCustomConfItem;
        try
        {
             emoCustomConfItem = readerWorkitem.ElementAt(0);
        }
        catch //(System.ArgumentOutOfRangeException)
        {
            return "CI with You sn not exist";
        }

//to method edit()

        //edit
        foreach (KeyValuePair<string, string> kvp in dsCustCIkeyVal)
        {
            if (kvp.Key != snkey)
            {
                try
                {
                    emoCustomConfItem[mpcCustomConfItem, kvp.Key].Value = kvp.Value; 
                }
                catch (ArgumentException)
                {
                    return "No such key in CI";
                }

                
            }
         }


    
 // to method save save()
        try
        {
            emoCustomConfItem.Commit();
            
        }
        catch
        {
            return "Cant commit changes maybee user not have permisions to save data";
        }

        return "end";       
      
    }
 



}